package fr.isen.java2.db.daos;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import fr.isen.java2.db.entities.Genre;

import javax.swing.plaf.nimbus.State;

public class GenreDao {

	public List<Genre> listGenres() {
		List<Genre> listOfGenre = new ArrayList<>();

		try (Connection connection = DataSourceFactory.getDataSource().getConnection()){
			try (Statement statement = connection.createStatement()){
				try(ResultSet results = statement.executeQuery("select * from genre")){
					while (results.next()){
						Genre genre = new Genre(results.getInt("idgenre"),results.getString("name"));
						listOfGenre.add(genre);
					}
				}
			}
		}
		catch (SQLException e){
			e.printStackTrace();;
		}
		return listOfGenre;
	}

	public Genre getGenre(String name) {
		try(Connection connection = DataSourceFactory.getDataSource().getConnection()){
			try(PreparedStatement statement = connection.prepareStatement("select * from genre where name=?")){
				statement.setString(1,name);
				try(ResultSet results = statement.executeQuery()){
					if (results.next()){
						return new Genre(results.getInt("idgenre"), results.getString("name"));
					}
				}
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return null;
	}

	public Genre addGenre(String name) {
		try(Connection connection = DataSourceFactory.getDataSource().getConnection()){
			String sqlQuery = "INSERT INTO genre(name)" + "VALUES(?)";
			try(PreparedStatement statement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)){
				statement.setString(1,name);
				statement.executeUpdate();
				ResultSet id = statement.getGeneratedKeys();
				if (id.next()){
					return new Genre(id.getInt(1),name);
				}
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return null;
	}
}
