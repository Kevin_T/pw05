package fr.isen.java2.db.daos;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.time.LocalDateTime;

import fr.isen.java2.db.entities.Film;
import fr.isen.java2.db.entities.Genre;

public class FilmDao {

	public List<Film> listFilms() {
		List<Film> listOfFilm = new ArrayList<>();

		try(Connection connection = DataSourceFactory.getDataSource().getConnection()){
			try(Statement statement = connection.createStatement()){
				try(ResultSet results = statement.executeQuery("SELECT * FROM film JOIN genre ON film.genre_id = genre.idgenre")){
					while (results.next()){
						java.sql.Date classicDate = results.getDate("release_date");
						LocalDate releaseDateTime = classicDate.toLocalDate();
						Genre genre = new Genre(results.getInt("genre_id"), results.getString("name"));
						Film film = new Film(results.getInt("idfilm"), results.getString("title"), releaseDateTime, genre ,results.getInt("duration"),results.getString("director"), results.getString("summary"));
						listOfFilm.add(film);
					}
				}
			}

		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return listOfFilm;
	}

	public List<Film> listFilmsByGenre(String genreName) {
		List<Film> filmByGenre = new ArrayList<>();
		try(Connection connection = DataSourceFactory.getDataSource().getConnection()){
			try(PreparedStatement statement = connection.prepareStatement("SELECT * FROM film JOIN genre ON film.genre_id = genre.idgenre WHERE genre.name = ?")){
				statement.setString(1,genreName);
				try (ResultSet results = statement.executeQuery()){
					while (results.next()){
						java.sql.Date classicDate = results.getDate("release_date");
						LocalDate releaseDateTime = classicDate.toLocalDate();
						Genre genre = new Genre(results.getInt("genre_id"), results.getString("name"));
						Film film = new Film(results.getInt("idfilm"), results.getString("title"), releaseDateTime, genre ,results.getInt("duration"),results.getString("director"), results.getString("summary"));
						filmByGenre.add(film);
					}
				}
			}


		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return filmByGenre;
	}

	public Film addFilm(Film film) {
		try(Connection connection = DataSourceFactory.getDataSource().getConnection()){
			String sqlQuery = "INSERT INTO film(title,release_date,genre_id,duration,director,summary) VALUES(?,?,?,?,?,?)";
			try(PreparedStatement statement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)){
				statement.setString(1,film.getTitle());
				statement.setDate(2, java.sql.Date.valueOf(film.getReleaseDate()));
				statement.setInt(3, film.getGenre().getId());
				statement.setInt(4,film.getDuration());
				statement.setString(5, film.getDirector());
				statement.setString(6, film.getSummary());
				statement.executeUpdate();

				ResultSet id = statement.getGeneratedKeys();
				if (id.next()){
					return new Film(id.getInt(1), film.getTitle(), film.getReleaseDate(), film.getGenre(), film.getDuration(), film.getDirector(), film.getSummary());
				}

			}

		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return null;
	}
}
