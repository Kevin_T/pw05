package fr.isen.java2.db.daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import fr.isen.java2.db.entities.Film;
import fr.isen.java2.db.entities.Genre;
import org.assertj.core.groups.Tuple;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;

import static org.assertj.core.api.Assertions.*;

public class FilmDaoTestCase {

	private FilmDao filmDao = new FilmDao();

	@Before
	public void initDb() throws Exception {
		Connection connection = DataSourceFactory.getDataSource().getConnection();
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(
				"CREATE TABLE IF NOT EXISTS genre (idgenre INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT , name VARCHAR(50) NOT NULL);");
		stmt.executeUpdate(
				"CREATE TABLE IF NOT EXISTS film (\r\n"
				+ "  idfilm INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\r\n" + "  title VARCHAR(100) NOT NULL,\r\n"
				+ "  release_date DATETIME NULL,\r\n" + "  genre_id INT NOT NULL,\r\n" + "  duration INT NULL,\r\n"
				+ "  director VARCHAR(100) NOT NULL,\r\n" + "  summary MEDIUMTEXT NULL,\r\n"
				+ "  CONSTRAINT genre_fk FOREIGN KEY (genre_id) REFERENCES genre (idgenre));");
		stmt.executeUpdate("DELETE FROM film");
		stmt.executeUpdate("DELETE FROM genre");
		stmt.executeUpdate("INSERT INTO genre(idgenre,name) VALUES (1,'Drama')");
		stmt.executeUpdate("INSERT INTO genre(idgenre,name) VALUES (2,'Comedy')");
		stmt.executeUpdate("INSERT INTO film(idfilm,title, release_date, genre_id, duration, director, summary) "
				+ "VALUES (1, 'Title 1', '2015-11-26 12:00:00.000', 1, 120, 'director 1', 'summary of the first film')");
		stmt.executeUpdate("INSERT INTO film(idfilm,title, release_date, genre_id, duration, director, summary) "
				+ "VALUES (2, 'My Title 2', '2015-11-14 12:00:00.000', 2, 114, 'director 2', 'summary of the second film')");
		stmt.executeUpdate("INSERT INTO film(idfilm,title, release_date, genre_id, duration, director, summary) "
				+ "VALUES (3, 'Third title', '2015-12-12 12:00:00.000', 2, 176, 'director 3', 'summary of the third film')");
		stmt.close();
		connection.close();
	}
	
	 @Test
	 public void shouldListFilms() {
		 //When
		 List<Film> film = filmDao.listFilms();
		 //Then
		 assertThat(film).hasSize(3);
		 assertThat(film).extracting("id", "title", "releaseDate","genre", "duration", "director", "summary").containsOnly(
				 tuple(1, "Title 1", LocalDate.of(2015,11,26), new Genre(1, "Drama"), 120, "director 1", "summary of the first film"),
				 tuple(2, "My Title 2", LocalDate.of(2015,11,14), new Genre(2, "Comedy"), 114, "director 2", "summary of the second film"),
				 tuple(3, "Third title", LocalDate.of(2015,12,12), new Genre(2, "Comedy"), 176, "director 3", "summary of the third film"));
	 }
	
	 @Test
	 public void shouldListFilmsByGenre() {
		 //WHEN
		 List<Film> film = filmDao.listFilmsByGenre("Drama");
		 //THEN
		 assertThat(film).hasSize(1);
		 assertThat(film).extracting("id", "title", "releaseDate","genre", "duration", "director", "summary").containsOnly(
				 tuple(1, "Title 1", LocalDate.of(2015,11,26), new Genre(1,"Drama"), 120, "director 1", "summary of the first film"));
	 }


	
	 @Test
	 public void shouldAddFilm() throws Exception {
		 //WHEN
		 filmDao.addFilm(new Film(4, "StarWars", LocalDate.of(1977,10,19), new Genre(3,"Fantasy"), 121, "George Lucas",
				 "StarWars Summary"));
		 //THEN
		 Connection connection = DataSourceFactory.getDataSource().getConnection();
		 Statement statement = connection.createStatement();
		 ResultSet resultSet = statement.executeQuery("SELECT * FROM film WHERE title='StarWars'");
		 assertThat(resultSet.next()).isTrue();
		 assertThat(resultSet.getInt("idfilm")).isNotNull();
		 assertThat(resultSet.getString("title")).isEqualTo("StarWars");
		 assertThat(resultSet.getDate("release_date")).isEqualTo(java.sql.Date.valueOf(LocalDate.of(1977,10,19)));
		 assertThat(resultSet.getInt("genre_id")).isEqualTo(3);
		 assertThat(resultSet.getInt("duration")).isEqualTo(121);
		 assertThat(resultSet.getString("director")).isEqualTo("George Lucas");
		 assertThat(resultSet.getString("summary")).isEqualTo("StarWars Summary");
		 assertThat(resultSet.next()).isFalse();
		 resultSet.close();
		 statement.close();
		 connection.close();
	 }
}
